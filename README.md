# scheduler
## master Branch
- [x] git add . <br> 
- [x] git commit -m "message"
- [x] git remote add origin GitHub-repository-URL
- [x] git push origin master
// -u to save perefrence to git repo
- [x] git push -u origin master
- [x] git remote rm origin // Delete remote acess [Recommended]
## dev Branch <br>
- [x] git remote add origin GitHub-repository-URL
- [x] git add . 
- [x] git commit -m "message"
- [x] git checkout -b dev
- [x] git push origin dev
// Delete remote acess [Recommended]
- [x] git remote rm origin
